﻿namespace GameSettings
{
	partial class Form1
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
		protected override void Dispose ( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose ();
			}
			base.Dispose ( disposing );
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent ()
		{
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.radioButton13 = new System.Windows.Forms.RadioButton();
			this.textBox13 = new System.Windows.Forms.TextBox();
			this.radioButton6 = new System.Windows.Forms.RadioButton();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.textBox14 = new System.Windows.Forms.TextBox();
			this.radioButton12 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.radioButton14 = new System.Windows.Forms.RadioButton();
			this.textBox12 = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.radioButton7 = new System.Windows.Forms.RadioButton();
			this.textBox15 = new System.Windows.Forms.TextBox();
			this.radioButton11 = new System.Windows.Forms.RadioButton();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.radioButton15 = new System.Windows.Forms.RadioButton();
			this.textBox11 = new System.Windows.Forms.TextBox();
			this.radioButton5 = new System.Windows.Forms.RadioButton();
			this.btnKeyConfigOK = new System.Windows.Forms.Button();
			this.radioButton16 = new System.Windows.Forms.RadioButton();
			this.radioButton10 = new System.Windows.Forms.RadioButton();
			this.textBox16 = new System.Windows.Forms.TextBox();
			this.radioButton8 = new System.Windows.Forms.RadioButton();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.textBox10 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.radioButton9 = new System.Windows.Forms.RadioButton();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.radioButton4 = new System.Windows.Forms.RadioButton();
			this.textBox9 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.btnKeyConfigCancel = new System.Windows.Forms.Button();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.cb_fullscreen = new System.Windows.Forms.CheckBox();
			this.btn_Cancel = new System.Windows.Forms.Button();
			this.cmb_resolution = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btn_OK = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage2.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabPage2
			// 
			this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPage2.Controls.Add(this.btnKeyConfigCancel);
			this.tabPage2.Controls.Add(this.textBox1);
			this.tabPage2.Controls.Add(this.textBox9);
			this.tabPage2.Controls.Add(this.textBox8);
			this.tabPage2.Controls.Add(this.textBox3);
			this.tabPage2.Controls.Add(this.textBox4);
			this.tabPage2.Controls.Add(this.textBox10);
			this.tabPage2.Controls.Add(this.textBox16);
			this.tabPage2.Controls.Add(this.textBox11);
			this.tabPage2.Controls.Add(this.textBox7);
			this.tabPage2.Controls.Add(this.textBox2);
			this.tabPage2.Controls.Add(this.textBox15);
			this.tabPage2.Controls.Add(this.textBox5);
			this.tabPage2.Controls.Add(this.textBox12);
			this.tabPage2.Controls.Add(this.textBox14);
			this.tabPage2.Controls.Add(this.textBox6);
			this.tabPage2.Controls.Add(this.textBox13);
			this.tabPage2.Controls.Add(this.radioButton4);
			this.tabPage2.Controls.Add(this.radioButton9);
			this.tabPage2.Controls.Add(this.radioButton3);
			this.tabPage2.Controls.Add(this.radioButton8);
			this.tabPage2.Controls.Add(this.radioButton10);
			this.tabPage2.Controls.Add(this.radioButton16);
			this.tabPage2.Controls.Add(this.btnKeyConfigOK);
			this.tabPage2.Controls.Add(this.radioButton5);
			this.tabPage2.Controls.Add(this.radioButton15);
			this.tabPage2.Controls.Add(this.radioButton11);
			this.tabPage2.Controls.Add(this.radioButton7);
			this.tabPage2.Controls.Add(this.radioButton14);
			this.tabPage2.Controls.Add(this.radioButton1);
			this.tabPage2.Controls.Add(this.radioButton2);
			this.tabPage2.Controls.Add(this.radioButton12);
			this.tabPage2.Controls.Add(this.radioButton6);
			this.tabPage2.Controls.Add(this.radioButton13);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(380, 292);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "キーコンフィグ";
			// 
			// radioButton13
			// 
			this.radioButton13.AutoSize = true;
			this.radioButton13.Location = new System.Drawing.Point(197, 98);
			this.radioButton13.Name = "radioButton13";
			this.radioButton13.Size = new System.Drawing.Size(47, 16);
			this.radioButton13.TabIndex = 81;
			this.radioButton13.Text = "key1";
			this.radioButton13.UseVisualStyleBackColor = true;
			// 
			// textBox13
			// 
			this.textBox13.Location = new System.Drawing.Point(247, 98);
			this.textBox13.Name = "textBox13";
			this.textBox13.ReadOnly = true;
			this.textBox13.Size = new System.Drawing.Size(108, 19);
			this.textBox13.TabIndex = 97;
			this.textBox13.TabStop = false;
			// 
			// radioButton6
			// 
			this.radioButton6.AutoSize = true;
			this.radioButton6.Location = new System.Drawing.Point(9, 121);
			this.radioButton6.Name = "radioButton6";
			this.radioButton6.Size = new System.Drawing.Size(47, 16);
			this.radioButton6.TabIndex = 74;
			this.radioButton6.Text = "key2";
			this.radioButton6.UseVisualStyleBackColor = true;
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(67, 121);
			this.textBox6.Name = "textBox6";
			this.textBox6.ReadOnly = true;
			this.textBox6.Size = new System.Drawing.Size(107, 19);
			this.textBox6.TabIndex = 92;
			this.textBox6.TabStop = false;
			// 
			// textBox14
			// 
			this.textBox14.Location = new System.Drawing.Point(247, 121);
			this.textBox14.Name = "textBox14";
			this.textBox14.ReadOnly = true;
			this.textBox14.Size = new System.Drawing.Size(108, 19);
			this.textBox14.TabIndex = 93;
			this.textBox14.TabStop = false;
			// 
			// radioButton12
			// 
			this.radioButton12.AutoSize = true;
			this.radioButton12.Location = new System.Drawing.Point(197, 75);
			this.radioButton12.Name = "radioButton12";
			this.radioButton12.Size = new System.Drawing.Size(46, 16);
			this.radioButton12.TabIndex = 80;
			this.radioButton12.Text = "right";
			this.radioButton12.UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this.radioButton2.AutoSize = true;
			this.radioButton2.Location = new System.Drawing.Point(9, 29);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(49, 16);
			this.radioButton2.TabIndex = 70;
			this.radioButton2.Text = "down";
			this.radioButton2.UseVisualStyleBackColor = true;
			// 
			// radioButton1
			// 
			this.radioButton1.AutoSize = true;
			this.radioButton1.Checked = true;
			this.radioButton1.Location = new System.Drawing.Point(9, 6);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(35, 16);
			this.radioButton1.TabIndex = 69;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "up";
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// radioButton14
			// 
			this.radioButton14.AutoSize = true;
			this.radioButton14.Location = new System.Drawing.Point(197, 121);
			this.radioButton14.Name = "radioButton14";
			this.radioButton14.Size = new System.Drawing.Size(47, 16);
			this.radioButton14.TabIndex = 82;
			this.radioButton14.Text = "key2";
			this.radioButton14.UseVisualStyleBackColor = true;
			// 
			// textBox12
			// 
			this.textBox12.Location = new System.Drawing.Point(247, 75);
			this.textBox12.Name = "textBox12";
			this.textBox12.ReadOnly = true;
			this.textBox12.Size = new System.Drawing.Size(107, 19);
			this.textBox12.TabIndex = 98;
			this.textBox12.TabStop = false;
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(67, 98);
			this.textBox5.Name = "textBox5";
			this.textBox5.ReadOnly = true;
			this.textBox5.Size = new System.Drawing.Size(107, 19);
			this.textBox5.TabIndex = 88;
			this.textBox5.TabStop = false;
			// 
			// radioButton7
			// 
			this.radioButton7.AutoSize = true;
			this.radioButton7.Location = new System.Drawing.Point(9, 144);
			this.radioButton7.Name = "radioButton7";
			this.radioButton7.Size = new System.Drawing.Size(47, 16);
			this.radioButton7.TabIndex = 75;
			this.radioButton7.Text = "key3";
			this.radioButton7.UseVisualStyleBackColor = true;
			// 
			// textBox15
			// 
			this.textBox15.Location = new System.Drawing.Point(247, 144);
			this.textBox15.Name = "textBox15";
			this.textBox15.ReadOnly = true;
			this.textBox15.Size = new System.Drawing.Size(108, 19);
			this.textBox15.TabIndex = 95;
			this.textBox15.TabStop = false;
			// 
			// radioButton11
			// 
			this.radioButton11.AutoSize = true;
			this.radioButton11.Location = new System.Drawing.Point(197, 52);
			this.radioButton11.Name = "radioButton11";
			this.radioButton11.Size = new System.Drawing.Size(40, 16);
			this.radioButton11.TabIndex = 79;
			this.radioButton11.Text = "left";
			this.radioButton11.UseVisualStyleBackColor = true;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(67, 29);
			this.textBox2.Name = "textBox2";
			this.textBox2.ReadOnly = true;
			this.textBox2.Size = new System.Drawing.Size(108, 19);
			this.textBox2.TabIndex = 100;
			this.textBox2.TabStop = false;
			// 
			// textBox7
			// 
			this.textBox7.Location = new System.Drawing.Point(67, 144);
			this.textBox7.Name = "textBox7";
			this.textBox7.ReadOnly = true;
			this.textBox7.Size = new System.Drawing.Size(107, 19);
			this.textBox7.TabIndex = 89;
			this.textBox7.TabStop = false;
			// 
			// radioButton15
			// 
			this.radioButton15.AutoSize = true;
			this.radioButton15.Location = new System.Drawing.Point(197, 144);
			this.radioButton15.Name = "radioButton15";
			this.radioButton15.Size = new System.Drawing.Size(47, 16);
			this.radioButton15.TabIndex = 83;
			this.radioButton15.Text = "key3";
			this.radioButton15.UseVisualStyleBackColor = true;
			// 
			// textBox11
			// 
			this.textBox11.Location = new System.Drawing.Point(247, 52);
			this.textBox11.Name = "textBox11";
			this.textBox11.ReadOnly = true;
			this.textBox11.Size = new System.Drawing.Size(107, 19);
			this.textBox11.TabIndex = 96;
			this.textBox11.TabStop = false;
			// 
			// radioButton5
			// 
			this.radioButton5.AutoSize = true;
			this.radioButton5.Location = new System.Drawing.Point(9, 98);
			this.radioButton5.Name = "radioButton5";
			this.radioButton5.Size = new System.Drawing.Size(47, 16);
			this.radioButton5.TabIndex = 73;
			this.radioButton5.Text = "key1";
			this.radioButton5.UseVisualStyleBackColor = true;
			// 
			// btnKeyConfigOK
			// 
			this.btnKeyConfigOK.Location = new System.Drawing.Point(249, 233);
			this.btnKeyConfigOK.Name = "btnKeyConfigOK";
			this.btnKeyConfigOK.Size = new System.Drawing.Size(106, 32);
			this.btnKeyConfigOK.TabIndex = 68;
			this.btnKeyConfigOK.Text = "決定";
			this.btnKeyConfigOK.UseVisualStyleBackColor = true;
			// 
			// radioButton16
			// 
			this.radioButton16.AutoSize = true;
			this.radioButton16.Location = new System.Drawing.Point(197, 167);
			this.radioButton16.Name = "radioButton16";
			this.radioButton16.Size = new System.Drawing.Size(47, 16);
			this.radioButton16.TabIndex = 84;
			this.radioButton16.Text = "key4";
			this.radioButton16.UseVisualStyleBackColor = true;
			// 
			// radioButton10
			// 
			this.radioButton10.AutoSize = true;
			this.radioButton10.Location = new System.Drawing.Point(197, 29);
			this.radioButton10.Name = "radioButton10";
			this.radioButton10.Size = new System.Drawing.Size(49, 16);
			this.radioButton10.TabIndex = 78;
			this.radioButton10.Text = "down";
			this.radioButton10.UseVisualStyleBackColor = true;
			// 
			// textBox16
			// 
			this.textBox16.Location = new System.Drawing.Point(247, 167);
			this.textBox16.Name = "textBox16";
			this.textBox16.ReadOnly = true;
			this.textBox16.Size = new System.Drawing.Size(108, 19);
			this.textBox16.TabIndex = 99;
			this.textBox16.TabStop = false;
			// 
			// radioButton8
			// 
			this.radioButton8.AutoSize = true;
			this.radioButton8.Location = new System.Drawing.Point(9, 167);
			this.radioButton8.Name = "radioButton8";
			this.radioButton8.Size = new System.Drawing.Size(47, 16);
			this.radioButton8.TabIndex = 76;
			this.radioButton8.Text = "key4";
			this.radioButton8.UseVisualStyleBackColor = true;
			// 
			// radioButton3
			// 
			this.radioButton3.AutoSize = true;
			this.radioButton3.Location = new System.Drawing.Point(9, 52);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(40, 16);
			this.radioButton3.TabIndex = 71;
			this.radioButton3.Text = "left";
			this.radioButton3.UseVisualStyleBackColor = true;
			// 
			// textBox10
			// 
			this.textBox10.Location = new System.Drawing.Point(247, 29);
			this.textBox10.Name = "textBox10";
			this.textBox10.ReadOnly = true;
			this.textBox10.Size = new System.Drawing.Size(107, 19);
			this.textBox10.TabIndex = 94;
			this.textBox10.TabStop = false;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(67, 75);
			this.textBox4.Name = "textBox4";
			this.textBox4.ReadOnly = true;
			this.textBox4.Size = new System.Drawing.Size(108, 19);
			this.textBox4.TabIndex = 85;
			this.textBox4.TabStop = false;
			// 
			// radioButton9
			// 
			this.radioButton9.AutoSize = true;
			this.radioButton9.Location = new System.Drawing.Point(197, 6);
			this.radioButton9.Name = "radioButton9";
			this.radioButton9.Size = new System.Drawing.Size(35, 16);
			this.radioButton9.TabIndex = 77;
			this.radioButton9.Text = "up";
			this.radioButton9.UseVisualStyleBackColor = true;
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(67, 52);
			this.textBox3.Name = "textBox3";
			this.textBox3.ReadOnly = true;
			this.textBox3.Size = new System.Drawing.Size(108, 19);
			this.textBox3.TabIndex = 87;
			this.textBox3.TabStop = false;
			// 
			// textBox8
			// 
			this.textBox8.Location = new System.Drawing.Point(67, 167);
			this.textBox8.Name = "textBox8";
			this.textBox8.ReadOnly = true;
			this.textBox8.Size = new System.Drawing.Size(108, 19);
			this.textBox8.TabIndex = 91;
			this.textBox8.TabStop = false;
			// 
			// radioButton4
			// 
			this.radioButton4.AutoSize = true;
			this.radioButton4.Location = new System.Drawing.Point(9, 75);
			this.radioButton4.Name = "radioButton4";
			this.radioButton4.Size = new System.Drawing.Size(46, 16);
			this.radioButton4.TabIndex = 72;
			this.radioButton4.Text = "right";
			this.radioButton4.UseVisualStyleBackColor = true;
			// 
			// textBox9
			// 
			this.textBox9.Location = new System.Drawing.Point(247, 6);
			this.textBox9.Name = "textBox9";
			this.textBox9.ReadOnly = true;
			this.textBox9.Size = new System.Drawing.Size(108, 19);
			this.textBox9.TabIndex = 90;
			this.textBox9.TabStop = false;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(67, 6);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.ShortcutsEnabled = false;
			this.textBox1.Size = new System.Drawing.Size(108, 19);
			this.textBox1.TabIndex = 86;
			this.textBox1.TabStop = false;
			// 
			// btnKeyConfigCancel
			// 
			this.btnKeyConfigCancel.Location = new System.Drawing.Point(122, 233);
			this.btnKeyConfigCancel.Name = "btnKeyConfigCancel";
			this.btnKeyConfigCancel.Size = new System.Drawing.Size(99, 32);
			this.btnKeyConfigCancel.TabIndex = 67;
			this.btnKeyConfigCancel.Text = "キャンセル";
			this.btnKeyConfigCancel.UseVisualStyleBackColor = true;
			// 
			// tabPage1
			// 
			this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPage1.Controls.Add(this.btn_OK);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.cmb_resolution);
			this.tabPage1.Controls.Add(this.btn_Cancel);
			this.tabPage1.Controls.Add(this.cb_fullscreen);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(380, 292);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "メイン";
			// 
			// cb_fullscreen
			// 
			this.cb_fullscreen.AutoSize = true;
			this.cb_fullscreen.Location = new System.Drawing.Point(3, 6);
			this.cb_fullscreen.Name = "cb_fullscreen";
			this.cb_fullscreen.Size = new System.Drawing.Size(85, 16);
			this.cb_fullscreen.TabIndex = 1;
			this.cb_fullscreen.Text = "フルスクリーン";
			this.cb_fullscreen.UseVisualStyleBackColor = true;
			this.cb_fullscreen.CheckedChanged += new System.EventHandler(this.cb_fullscreen_CheckedChanged);
			// 
			// btn_Cancel
			// 
			this.btn_Cancel.BackColor = System.Drawing.Color.MistyRose;
			this.btn_Cancel.Location = new System.Drawing.Point(17, 154);
			this.btn_Cancel.Name = "btn_Cancel";
			this.btn_Cancel.Size = new System.Drawing.Size(113, 33);
			this.btn_Cancel.TabIndex = 3;
			this.btn_Cancel.Text = "キャンセルして終了";
			this.btn_Cancel.UseVisualStyleBackColor = false;
			this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
			// 
			// cmb_resolution
			// 
			this.cmb_resolution.BackColor = System.Drawing.SystemColors.Control;
			this.cmb_resolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmb_resolution.FormattingEnabled = true;
			this.cmb_resolution.Location = new System.Drawing.Point(6, 54);
			this.cmb_resolution.Name = "cmb_resolution";
			this.cmb_resolution.Size = new System.Drawing.Size(168, 20);
			this.cmb_resolution.TabIndex = 4;
			this.cmb_resolution.SelectedIndexChanged += new System.EventHandler(this.cmb_resolution_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 39);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(77, 12);
			this.label1.TabIndex = 5;
			this.label1.Text = "ウィンドウサイズ";
			// 
			// btn_OK
			// 
			this.btn_OK.BackColor = System.Drawing.Color.LightBlue;
			this.btn_OK.Location = new System.Drawing.Point(160, 123);
			this.btn_OK.Name = "btn_OK";
			this.btn_OK.Size = new System.Drawing.Size(104, 64);
			this.btn_OK.TabIndex = 2;
			this.btn_OK.Text = "保存して終了";
			this.btn_OK.UseVisualStyleBackColor = false;
			this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Location = new System.Drawing.Point(2, 2);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(388, 318);
			this.tabControl1.TabIndex = 6;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(393, 321);
			this.Controls.Add(this.tabControl1);
			this.Name = "Form1";
			this.Text = "設定";
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button btnKeyConfigCancel;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox9;
		private System.Windows.Forms.TextBox textBox8;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox10;
		private System.Windows.Forms.TextBox textBox16;
		private System.Windows.Forms.TextBox textBox11;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox15;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.TextBox textBox12;
		private System.Windows.Forms.TextBox textBox14;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.TextBox textBox13;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.RadioButton radioButton9;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.RadioButton radioButton8;
		private System.Windows.Forms.RadioButton radioButton10;
		private System.Windows.Forms.RadioButton radioButton16;
		private System.Windows.Forms.Button btnKeyConfigOK;
		private System.Windows.Forms.RadioButton radioButton5;
		private System.Windows.Forms.RadioButton radioButton15;
		private System.Windows.Forms.RadioButton radioButton11;
		private System.Windows.Forms.RadioButton radioButton7;
		private System.Windows.Forms.RadioButton radioButton14;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton12;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.RadioButton radioButton13;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.Button btn_OK;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmb_resolution;
		private System.Windows.Forms.Button btn_Cancel;
		private System.Windows.Forms.CheckBox cb_fullscreen;
		private System.Windows.Forms.TabControl tabControl1;
	}
}

