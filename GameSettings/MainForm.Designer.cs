﻿namespace GameSettings
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose ( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose ();
			}
			base.Dispose ( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.label3 = new System.Windows.Forms.Label();
			this.CH_StartBattle = new System.Windows.Forms.CheckBox();
			this.CH_Training = new System.Windows.Forms.CheckBox();
			this.CH_Demo = new System.Windows.Forms.CheckBox();
			this.btn_folder = new System.Windows.Forms.Button();
			this.lbl_1p = new System.Windows.Forms.Label();
			this.btn_OK = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.cmb_resolution = new System.Windows.Forms.ComboBox();
			this.btn_Cancel = new System.Windows.Forms.Button();
			this.cb_fullscreen = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.cB_Chara1p = new System.Windows.Forms.ComboBox();
			this.lbl_Chara1p = new System.Windows.Forms.Label();
			this.lbl_input = new System.Windows.Forms.Label();
			this.rb_input1pCPU = new System.Windows.Forms.RadioButton();
			this.rb_input1pPlayer = new System.Windows.Forms.RadioButton();
			this.panel2 = new System.Windows.Forms.Panel();
			this.cB_Chara2p = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.rb_input2pCPU = new System.Windows.Forms.RadioButton();
			this.rb_input2pPlayer = new System.Windows.Forms.RadioButton();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.btnKeyConfigCancel = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox9 = new System.Windows.Forms.TextBox();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox10 = new System.Windows.Forms.TextBox();
			this.textBox16 = new System.Windows.Forms.TextBox();
			this.textBox11 = new System.Windows.Forms.TextBox();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox15 = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.textBox12 = new System.Windows.Forms.TextBox();
			this.textBox14 = new System.Windows.Forms.TextBox();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.textBox13 = new System.Windows.Forms.TextBox();
			this.radioButton4 = new System.Windows.Forms.RadioButton();
			this.radioButton9 = new System.Windows.Forms.RadioButton();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.radioButton8 = new System.Windows.Forms.RadioButton();
			this.radioButton10 = new System.Windows.Forms.RadioButton();
			this.radioButton16 = new System.Windows.Forms.RadioButton();
			this.btnKeyConfigOK = new System.Windows.Forms.Button();
			this.radioButton5 = new System.Windows.Forms.RadioButton();
			this.radioButton15 = new System.Windows.Forms.RadioButton();
			this.radioButton11 = new System.Windows.Forms.RadioButton();
			this.radioButton7 = new System.Windows.Forms.RadioButton();
			this.radioButton14 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton12 = new System.Windows.Forms.RadioButton();
			this.radioButton6 = new System.Windows.Forms.RadioButton();
			this.radioButton13 = new System.Windows.Forms.RadioButton();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(385, 355);
			this.tabControl1.TabIndex = 7;
			this.tabControl1.TabStop = false;
			this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
			this.tabControl1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.tabControl1_PreviewKeyDown);
			// 
			// tabPage1
			// 
			this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.CH_StartBattle);
			this.tabPage1.Controls.Add(this.CH_Training);
			this.tabPage1.Controls.Add(this.CH_Demo);
			this.tabPage1.Controls.Add(this.btn_folder);
			this.tabPage1.Controls.Add(this.lbl_1p);
			this.tabPage1.Controls.Add(this.btn_OK);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.cmb_resolution);
			this.tabPage1.Controls.Add(this.btn_Cancel);
			this.tabPage1.Controls.Add(this.cb_fullscreen);
			this.tabPage1.Controls.Add(this.panel1);
			this.tabPage1.Controls.Add(this.panel2);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(377, 329);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "メイン";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(27, 187);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(18, 12);
			this.label3.TabIndex = 6;
			this.label3.Text = "2P";
			// 
			// CH_StartBattle
			// 
			this.CH_StartBattle.AutoSize = true;
			this.CH_StartBattle.Location = new System.Drawing.Point(109, 51);
			this.CH_StartBattle.Name = "CH_StartBattle";
			this.CH_StartBattle.Size = new System.Drawing.Size(94, 16);
			this.CH_StartBattle.TabIndex = 12;
			this.CH_StartBattle.Text = "バトルから開始";
			this.CH_StartBattle.UseVisualStyleBackColor = true;
			this.CH_StartBattle.CheckedChanged += new System.EventHandler(this.CH_StartBattle_CheckedChanged);
			// 
			// CH_Training
			// 
			this.CH_Training.AutoSize = true;
			this.CH_Training.Location = new System.Drawing.Point(221, 51);
			this.CH_Training.Name = "CH_Training";
			this.CH_Training.Size = new System.Drawing.Size(106, 16);
			this.CH_Training.TabIndex = 12;
			this.CH_Training.Text = "トレーニングモード";
			this.CH_Training.UseVisualStyleBackColor = true;
			this.CH_Training.CheckedChanged += new System.EventHandler(this.CH_Training_CheckedChanged);
			// 
			// CH_Demo
			// 
			this.CH_Demo.AutoSize = true;
			this.CH_Demo.Location = new System.Drawing.Point(16, 51);
			this.CH_Demo.Name = "CH_Demo";
			this.CH_Demo.Size = new System.Drawing.Size(71, 16);
			this.CH_Demo.TabIndex = 11;
			this.CH_Demo.Text = "デモモード";
			this.CH_Demo.UseVisualStyleBackColor = true;
			this.CH_Demo.CheckedChanged += new System.EventHandler(this.CH_Demo_CheckedChanged);
			// 
			// btn_folder
			// 
			this.btn_folder.Location = new System.Drawing.Point(17, 287);
			this.btn_folder.Name = "btn_folder";
			this.btn_folder.Size = new System.Drawing.Size(96, 33);
			this.btn_folder.TabIndex = 10;
			this.btn_folder.Text = "フォルダを開く";
			this.btn_folder.UseVisualStyleBackColor = true;
			this.btn_folder.Click += new System.EventHandler(this.btn_folder_Click);
			// 
			// lbl_1p
			// 
			this.lbl_1p.AutoSize = true;
			this.lbl_1p.Location = new System.Drawing.Point(27, 89);
			this.lbl_1p.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lbl_1p.Name = "lbl_1p";
			this.lbl_1p.Size = new System.Drawing.Size(18, 12);
			this.lbl_1p.TabIndex = 6;
			this.lbl_1p.Text = "1P";
			// 
			// btn_OK
			// 
			this.btn_OK.BackColor = System.Drawing.Color.LightBlue;
			this.btn_OK.Location = new System.Drawing.Point(250, 265);
			this.btn_OK.Name = "btn_OK";
			this.btn_OK.Size = new System.Drawing.Size(108, 55);
			this.btn_OK.TabIndex = 5;
			this.btn_OK.Text = "保存して終了";
			this.btn_OK.UseVisualStyleBackColor = false;
			this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(117, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(77, 12);
			this.label1.TabIndex = 5;
			this.label1.Text = "ウィンドウサイズ";
			// 
			// cmb_resolution
			// 
			this.cmb_resolution.BackColor = System.Drawing.SystemColors.Control;
			this.cmb_resolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmb_resolution.FormattingEnabled = true;
			this.cmb_resolution.Location = new System.Drawing.Point(200, 14);
			this.cmb_resolution.Name = "cmb_resolution";
			this.cmb_resolution.Size = new System.Drawing.Size(100, 20);
			this.cmb_resolution.TabIndex = 1;
			this.cmb_resolution.SelectedIndexChanged += new System.EventHandler(this.cmb_resolution_SelectedIndexChanged);
			// 
			// btn_Cancel
			// 
			this.btn_Cancel.BackColor = System.Drawing.Color.MistyRose;
			this.btn_Cancel.Location = new System.Drawing.Point(131, 287);
			this.btn_Cancel.Name = "btn_Cancel";
			this.btn_Cancel.Size = new System.Drawing.Size(113, 33);
			this.btn_Cancel.TabIndex = 4;
			this.btn_Cancel.Text = "キャンセルして終了";
			this.btn_Cancel.UseVisualStyleBackColor = false;
			this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
			// 
			// cb_fullscreen
			// 
			this.cb_fullscreen.AutoSize = true;
			this.cb_fullscreen.Location = new System.Drawing.Point(17, 16);
			this.cb_fullscreen.Name = "cb_fullscreen";
			this.cb_fullscreen.Size = new System.Drawing.Size(85, 16);
			this.cb_fullscreen.TabIndex = 0;
			this.cb_fullscreen.Text = "フルスクリーン";
			this.cb_fullscreen.UseVisualStyleBackColor = true;
			this.cb_fullscreen.CheckedChanged += new System.EventHandler(this.cb_fullscreen_CheckedChanged);
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.cB_Chara1p);
			this.panel1.Controls.Add(this.lbl_Chara1p);
			this.panel1.Controls.Add(this.lbl_input);
			this.panel1.Controls.Add(this.rb_input1pCPU);
			this.panel1.Controls.Add(this.rb_input1pPlayer);
			this.panel1.Location = new System.Drawing.Point(16, 91);
			this.panel1.Margin = new System.Windows.Forms.Padding(2);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(342, 67);
			this.panel1.TabIndex = 9;
			// 
			// cB_Chara1p
			// 
			this.cB_Chara1p.FormattingEnabled = true;
			this.cB_Chara1p.Location = new System.Drawing.Point(204, 15);
			this.cB_Chara1p.Name = "cB_Chara1p";
			this.cB_Chara1p.Size = new System.Drawing.Size(121, 20);
			this.cB_Chara1p.TabIndex = 10;
			this.cB_Chara1p.SelectedIndexChanged += new System.EventHandler(this.cB_Chara1p_SelectedIndexChanged);
			// 
			// lbl_Chara1p
			// 
			this.lbl_Chara1p.AutoSize = true;
			this.lbl_Chara1p.Location = new System.Drawing.Point(167, 18);
			this.lbl_Chara1p.Name = "lbl_Chara1p";
			this.lbl_Chara1p.Size = new System.Drawing.Size(31, 12);
			this.lbl_Chara1p.TabIndex = 9;
			this.lbl_Chara1p.Text = "キャラ";
			// 
			// lbl_input
			// 
			this.lbl_input.AutoSize = true;
			this.lbl_input.Location = new System.Drawing.Point(10, 18);
			this.lbl_input.Name = "lbl_input";
			this.lbl_input.Size = new System.Drawing.Size(29, 12);
			this.lbl_input.TabIndex = 9;
			this.lbl_input.Text = "操作";
			// 
			// rb_input1pCPU
			// 
			this.rb_input1pCPU.AutoSize = true;
			this.rb_input1pCPU.Location = new System.Drawing.Point(50, 36);
			this.rb_input1pCPU.Margin = new System.Windows.Forms.Padding(2);
			this.rb_input1pCPU.Name = "rb_input1pCPU";
			this.rb_input1pCPU.Size = new System.Drawing.Size(46, 16);
			this.rb_input1pCPU.TabIndex = 8;
			this.rb_input1pCPU.TabStop = true;
			this.rb_input1pCPU.Text = "CPU";
			this.rb_input1pCPU.UseVisualStyleBackColor = true;
			// 
			// rb_input1pPlayer
			// 
			this.rb_input1pPlayer.AutoSize = true;
			this.rb_input1pPlayer.Checked = true;
			this.rb_input1pPlayer.Location = new System.Drawing.Point(50, 16);
			this.rb_input1pPlayer.Margin = new System.Windows.Forms.Padding(2);
			this.rb_input1pPlayer.Name = "rb_input1pPlayer";
			this.rb_input1pPlayer.Size = new System.Drawing.Size(70, 16);
			this.rb_input1pPlayer.TabIndex = 2;
			this.rb_input1pPlayer.TabStop = true;
			this.rb_input1pPlayer.Text = "プレイヤー";
			this.rb_input1pPlayer.UseVisualStyleBackColor = true;
			this.rb_input1pPlayer.CheckedChanged += new System.EventHandler(this.rb_input1pPlayer_CheckedChanged);
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.cB_Chara2p);
			this.panel2.Controls.Add(this.label5);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.rb_input2pCPU);
			this.panel2.Controls.Add(this.rb_input2pPlayer);
			this.panel2.Location = new System.Drawing.Point(17, 187);
			this.panel2.Margin = new System.Windows.Forms.Padding(2);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(341, 69);
			this.panel2.TabIndex = 9;
			// 
			// cB_Chara2p
			// 
			this.cB_Chara2p.FormattingEnabled = true;
			this.cB_Chara2p.Location = new System.Drawing.Point(203, 12);
			this.cB_Chara2p.Name = "cB_Chara2p";
			this.cB_Chara2p.Size = new System.Drawing.Size(121, 20);
			this.cB_Chara2p.TabIndex = 10;
			this.cB_Chara2p.SelectedIndexChanged += new System.EventHandler(this.cB_Chara2p_SelectedIndexChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(166, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(31, 12);
			this.label5.TabIndex = 9;
			this.label5.Text = "キャラ";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(9, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(29, 12);
			this.label4.TabIndex = 9;
			this.label4.Text = "操作";
			// 
			// rb_input2pCPU
			// 
			this.rb_input2pCPU.AutoSize = true;
			this.rb_input2pCPU.Location = new System.Drawing.Point(49, 33);
			this.rb_input2pCPU.Margin = new System.Windows.Forms.Padding(2);
			this.rb_input2pCPU.Name = "rb_input2pCPU";
			this.rb_input2pCPU.Size = new System.Drawing.Size(46, 16);
			this.rb_input2pCPU.TabIndex = 8;
			this.rb_input2pCPU.TabStop = true;
			this.rb_input2pCPU.Text = "CPU";
			this.rb_input2pCPU.UseVisualStyleBackColor = true;
			// 
			// rb_input2pPlayer
			// 
			this.rb_input2pPlayer.AutoSize = true;
			this.rb_input2pPlayer.Checked = true;
			this.rb_input2pPlayer.Location = new System.Drawing.Point(48, 13);
			this.rb_input2pPlayer.Margin = new System.Windows.Forms.Padding(2);
			this.rb_input2pPlayer.Name = "rb_input2pPlayer";
			this.rb_input2pPlayer.Size = new System.Drawing.Size(70, 16);
			this.rb_input2pPlayer.TabIndex = 3;
			this.rb_input2pPlayer.TabStop = true;
			this.rb_input2pPlayer.Text = "プレイヤー";
			this.rb_input2pPlayer.UseVisualStyleBackColor = true;
			this.rb_input2pPlayer.CheckedChanged += new System.EventHandler(this.rb_input2pPlayer_CheckedChanged);
			// 
			// tabPage2
			// 
			this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPage2.Controls.Add(this.btnKeyConfigCancel);
			this.tabPage2.Controls.Add(this.textBox1);
			this.tabPage2.Controls.Add(this.textBox9);
			this.tabPage2.Controls.Add(this.textBox8);
			this.tabPage2.Controls.Add(this.textBox3);
			this.tabPage2.Controls.Add(this.textBox4);
			this.tabPage2.Controls.Add(this.textBox10);
			this.tabPage2.Controls.Add(this.textBox16);
			this.tabPage2.Controls.Add(this.textBox11);
			this.tabPage2.Controls.Add(this.textBox7);
			this.tabPage2.Controls.Add(this.textBox2);
			this.tabPage2.Controls.Add(this.textBox15);
			this.tabPage2.Controls.Add(this.textBox5);
			this.tabPage2.Controls.Add(this.textBox12);
			this.tabPage2.Controls.Add(this.textBox14);
			this.tabPage2.Controls.Add(this.textBox6);
			this.tabPage2.Controls.Add(this.textBox13);
			this.tabPage2.Controls.Add(this.radioButton4);
			this.tabPage2.Controls.Add(this.radioButton9);
			this.tabPage2.Controls.Add(this.radioButton3);
			this.tabPage2.Controls.Add(this.radioButton8);
			this.tabPage2.Controls.Add(this.radioButton10);
			this.tabPage2.Controls.Add(this.radioButton16);
			this.tabPage2.Controls.Add(this.btnKeyConfigOK);
			this.tabPage2.Controls.Add(this.radioButton5);
			this.tabPage2.Controls.Add(this.radioButton15);
			this.tabPage2.Controls.Add(this.radioButton11);
			this.tabPage2.Controls.Add(this.radioButton7);
			this.tabPage2.Controls.Add(this.radioButton14);
			this.tabPage2.Controls.Add(this.radioButton1);
			this.tabPage2.Controls.Add(this.radioButton2);
			this.tabPage2.Controls.Add(this.radioButton12);
			this.tabPage2.Controls.Add(this.radioButton6);
			this.tabPage2.Controls.Add(this.radioButton13);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(377, 329);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "キーコンフィグ";
			// 
			// btnKeyConfigCancel
			// 
			this.btnKeyConfigCancel.BackColor = System.Drawing.Color.MistyRose;
			this.btnKeyConfigCancel.Location = new System.Drawing.Point(94, 288);
			this.btnKeyConfigCancel.Name = "btnKeyConfigCancel";
			this.btnKeyConfigCancel.Size = new System.Drawing.Size(138, 32);
			this.btnKeyConfigCancel.TabIndex = 67;
			this.btnKeyConfigCancel.Text = "キャンセルして終了";
			this.btnKeyConfigCancel.UseVisualStyleBackColor = false;
			this.btnKeyConfigCancel.Click += new System.EventHandler(this.btnKeyConfigCancel_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(67, 21);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.ShortcutsEnabled = false;
			this.textBox1.Size = new System.Drawing.Size(108, 19);
			this.textBox1.TabIndex = 86;
			this.textBox1.TabStop = false;
			// 
			// textBox9
			// 
			this.textBox9.Location = new System.Drawing.Point(247, 21);
			this.textBox9.Name = "textBox9";
			this.textBox9.ReadOnly = true;
			this.textBox9.Size = new System.Drawing.Size(108, 19);
			this.textBox9.TabIndex = 90;
			this.textBox9.TabStop = false;
			// 
			// textBox8
			// 
			this.textBox8.Location = new System.Drawing.Point(67, 182);
			this.textBox8.Name = "textBox8";
			this.textBox8.ReadOnly = true;
			this.textBox8.Size = new System.Drawing.Size(108, 19);
			this.textBox8.TabIndex = 91;
			this.textBox8.TabStop = false;
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(67, 67);
			this.textBox3.Name = "textBox3";
			this.textBox3.ReadOnly = true;
			this.textBox3.Size = new System.Drawing.Size(108, 19);
			this.textBox3.TabIndex = 87;
			this.textBox3.TabStop = false;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(67, 90);
			this.textBox4.Name = "textBox4";
			this.textBox4.ReadOnly = true;
			this.textBox4.Size = new System.Drawing.Size(108, 19);
			this.textBox4.TabIndex = 85;
			this.textBox4.TabStop = false;
			// 
			// textBox10
			// 
			this.textBox10.Location = new System.Drawing.Point(247, 44);
			this.textBox10.Name = "textBox10";
			this.textBox10.ReadOnly = true;
			this.textBox10.Size = new System.Drawing.Size(107, 19);
			this.textBox10.TabIndex = 94;
			this.textBox10.TabStop = false;
			// 
			// textBox16
			// 
			this.textBox16.Location = new System.Drawing.Point(247, 182);
			this.textBox16.Name = "textBox16";
			this.textBox16.ReadOnly = true;
			this.textBox16.Size = new System.Drawing.Size(108, 19);
			this.textBox16.TabIndex = 99;
			this.textBox16.TabStop = false;
			// 
			// textBox11
			// 
			this.textBox11.Location = new System.Drawing.Point(247, 67);
			this.textBox11.Name = "textBox11";
			this.textBox11.ReadOnly = true;
			this.textBox11.Size = new System.Drawing.Size(107, 19);
			this.textBox11.TabIndex = 96;
			this.textBox11.TabStop = false;
			// 
			// textBox7
			// 
			this.textBox7.Location = new System.Drawing.Point(67, 159);
			this.textBox7.Name = "textBox7";
			this.textBox7.ReadOnly = true;
			this.textBox7.Size = new System.Drawing.Size(107, 19);
			this.textBox7.TabIndex = 89;
			this.textBox7.TabStop = false;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(67, 44);
			this.textBox2.Name = "textBox2";
			this.textBox2.ReadOnly = true;
			this.textBox2.Size = new System.Drawing.Size(108, 19);
			this.textBox2.TabIndex = 100;
			this.textBox2.TabStop = false;
			// 
			// textBox15
			// 
			this.textBox15.Location = new System.Drawing.Point(247, 159);
			this.textBox15.Name = "textBox15";
			this.textBox15.ReadOnly = true;
			this.textBox15.Size = new System.Drawing.Size(108, 19);
			this.textBox15.TabIndex = 95;
			this.textBox15.TabStop = false;
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(67, 113);
			this.textBox5.Name = "textBox5";
			this.textBox5.ReadOnly = true;
			this.textBox5.Size = new System.Drawing.Size(107, 19);
			this.textBox5.TabIndex = 88;
			this.textBox5.TabStop = false;
			// 
			// textBox12
			// 
			this.textBox12.Location = new System.Drawing.Point(247, 90);
			this.textBox12.Name = "textBox12";
			this.textBox12.ReadOnly = true;
			this.textBox12.Size = new System.Drawing.Size(107, 19);
			this.textBox12.TabIndex = 98;
			this.textBox12.TabStop = false;
			// 
			// textBox14
			// 
			this.textBox14.Location = new System.Drawing.Point(247, 136);
			this.textBox14.Name = "textBox14";
			this.textBox14.ReadOnly = true;
			this.textBox14.Size = new System.Drawing.Size(108, 19);
			this.textBox14.TabIndex = 93;
			this.textBox14.TabStop = false;
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(67, 136);
			this.textBox6.Name = "textBox6";
			this.textBox6.ReadOnly = true;
			this.textBox6.Size = new System.Drawing.Size(107, 19);
			this.textBox6.TabIndex = 92;
			this.textBox6.TabStop = false;
			// 
			// textBox13
			// 
			this.textBox13.Location = new System.Drawing.Point(247, 113);
			this.textBox13.Name = "textBox13";
			this.textBox13.ReadOnly = true;
			this.textBox13.Size = new System.Drawing.Size(108, 19);
			this.textBox13.TabIndex = 97;
			this.textBox13.TabStop = false;
			// 
			// radioButton4
			// 
			this.radioButton4.AutoSize = true;
			this.radioButton4.Location = new System.Drawing.Point(9, 90);
			this.radioButton4.Name = "radioButton4";
			this.radioButton4.Size = new System.Drawing.Size(46, 16);
			this.radioButton4.TabIndex = 72;
			this.radioButton4.Text = "right";
			this.radioButton4.UseVisualStyleBackColor = true;
			this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton4.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton9
			// 
			this.radioButton9.AutoSize = true;
			this.radioButton9.Location = new System.Drawing.Point(197, 21);
			this.radioButton9.Name = "radioButton9";
			this.radioButton9.Size = new System.Drawing.Size(35, 16);
			this.radioButton9.TabIndex = 77;
			this.radioButton9.Text = "up";
			this.radioButton9.UseVisualStyleBackColor = true;
			this.radioButton9.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton9.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton3
			// 
			this.radioButton3.AutoSize = true;
			this.radioButton3.Location = new System.Drawing.Point(9, 67);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(40, 16);
			this.radioButton3.TabIndex = 71;
			this.radioButton3.Text = "left";
			this.radioButton3.UseVisualStyleBackColor = true;
			this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton8
			// 
			this.radioButton8.AutoSize = true;
			this.radioButton8.Location = new System.Drawing.Point(9, 182);
			this.radioButton8.Name = "radioButton8";
			this.radioButton8.Size = new System.Drawing.Size(47, 16);
			this.radioButton8.TabIndex = 76;
			this.radioButton8.Text = "key4";
			this.radioButton8.UseVisualStyleBackColor = true;
			this.radioButton8.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton8.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton10
			// 
			this.radioButton10.AutoSize = true;
			this.radioButton10.Location = new System.Drawing.Point(197, 44);
			this.radioButton10.Name = "radioButton10";
			this.radioButton10.Size = new System.Drawing.Size(49, 16);
			this.radioButton10.TabIndex = 78;
			this.radioButton10.Text = "down";
			this.radioButton10.UseVisualStyleBackColor = true;
			this.radioButton10.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton10.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton16
			// 
			this.radioButton16.AutoSize = true;
			this.radioButton16.Location = new System.Drawing.Point(197, 182);
			this.radioButton16.Name = "radioButton16";
			this.radioButton16.Size = new System.Drawing.Size(47, 16);
			this.radioButton16.TabIndex = 84;
			this.radioButton16.Text = "key4";
			this.radioButton16.UseVisualStyleBackColor = true;
			this.radioButton16.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton16.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// btnKeyConfigOK
			// 
			this.btnKeyConfigOK.BackColor = System.Drawing.Color.LightBlue;
			this.btnKeyConfigOK.Location = new System.Drawing.Point(249, 263);
			this.btnKeyConfigOK.Name = "btnKeyConfigOK";
			this.btnKeyConfigOK.Size = new System.Drawing.Size(106, 57);
			this.btnKeyConfigOK.TabIndex = 68;
			this.btnKeyConfigOK.Text = "保存して終了";
			this.btnKeyConfigOK.UseVisualStyleBackColor = false;
			this.btnKeyConfigOK.Click += new System.EventHandler(this.btnKeyConfigOK_Click);
			// 
			// radioButton5
			// 
			this.radioButton5.AutoSize = true;
			this.radioButton5.Location = new System.Drawing.Point(9, 113);
			this.radioButton5.Name = "radioButton5";
			this.radioButton5.Size = new System.Drawing.Size(47, 16);
			this.radioButton5.TabIndex = 73;
			this.radioButton5.Text = "key1";
			this.radioButton5.UseVisualStyleBackColor = true;
			this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton5.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton15
			// 
			this.radioButton15.AutoSize = true;
			this.radioButton15.Location = new System.Drawing.Point(197, 159);
			this.radioButton15.Name = "radioButton15";
			this.radioButton15.Size = new System.Drawing.Size(47, 16);
			this.radioButton15.TabIndex = 83;
			this.radioButton15.Text = "key3";
			this.radioButton15.UseVisualStyleBackColor = true;
			this.radioButton15.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton15.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton11
			// 
			this.radioButton11.AutoSize = true;
			this.radioButton11.Location = new System.Drawing.Point(197, 67);
			this.radioButton11.Name = "radioButton11";
			this.radioButton11.Size = new System.Drawing.Size(40, 16);
			this.radioButton11.TabIndex = 79;
			this.radioButton11.Text = "left";
			this.radioButton11.UseVisualStyleBackColor = true;
			this.radioButton11.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton11.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton7
			// 
			this.radioButton7.AutoSize = true;
			this.radioButton7.Location = new System.Drawing.Point(9, 159);
			this.radioButton7.Name = "radioButton7";
			this.radioButton7.Size = new System.Drawing.Size(47, 16);
			this.radioButton7.TabIndex = 75;
			this.radioButton7.Text = "key3";
			this.radioButton7.UseVisualStyleBackColor = true;
			this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton7.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton14
			// 
			this.radioButton14.AutoSize = true;
			this.radioButton14.Location = new System.Drawing.Point(197, 136);
			this.radioButton14.Name = "radioButton14";
			this.radioButton14.Size = new System.Drawing.Size(47, 16);
			this.radioButton14.TabIndex = 82;
			this.radioButton14.Text = "key2";
			this.radioButton14.UseVisualStyleBackColor = true;
			this.radioButton14.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton14.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton1
			// 
			this.radioButton1.AutoSize = true;
			this.radioButton1.Checked = true;
			this.radioButton1.Location = new System.Drawing.Point(9, 21);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(35, 16);
			this.radioButton1.TabIndex = 69;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "up";
			this.radioButton1.UseVisualStyleBackColor = true;
			this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton2
			// 
			this.radioButton2.AutoSize = true;
			this.radioButton2.Location = new System.Drawing.Point(9, 44);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(49, 16);
			this.radioButton2.TabIndex = 70;
			this.radioButton2.Text = "down";
			this.radioButton2.UseVisualStyleBackColor = true;
			this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton12
			// 
			this.radioButton12.AutoSize = true;
			this.radioButton12.Location = new System.Drawing.Point(197, 90);
			this.radioButton12.Name = "radioButton12";
			this.radioButton12.Size = new System.Drawing.Size(46, 16);
			this.radioButton12.TabIndex = 80;
			this.radioButton12.Text = "right";
			this.radioButton12.UseVisualStyleBackColor = true;
			this.radioButton12.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton12.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton6
			// 
			this.radioButton6.AutoSize = true;
			this.radioButton6.Location = new System.Drawing.Point(9, 136);
			this.radioButton6.Name = "radioButton6";
			this.radioButton6.Size = new System.Drawing.Size(47, 16);
			this.radioButton6.TabIndex = 74;
			this.radioButton6.Text = "key2";
			this.radioButton6.UseVisualStyleBackColor = true;
			this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton6.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// radioButton13
			// 
			this.radioButton13.AutoSize = true;
			this.radioButton13.Location = new System.Drawing.Point(197, 113);
			this.radioButton13.Name = "radioButton13";
			this.radioButton13.Size = new System.Drawing.Size(47, 16);
			this.radioButton13.TabIndex = 81;
			this.radioButton13.Text = "key1";
			this.radioButton13.UseVisualStyleBackColor = true;
			this.radioButton13.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButton13.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.radioButton1_PreviewKeyDown);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(386, 354);
			this.Controls.Add(this.tabControl1);
			this.Name = "MainForm";
			this.Text = "設定";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.Button btn_OK;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmb_resolution;
		private System.Windows.Forms.Button btn_Cancel;
		private System.Windows.Forms.CheckBox cb_fullscreen;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button btnKeyConfigCancel;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox9;
		private System.Windows.Forms.TextBox textBox8;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox10;
		private System.Windows.Forms.TextBox textBox16;
		private System.Windows.Forms.TextBox textBox11;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox15;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.TextBox textBox12;
		private System.Windows.Forms.TextBox textBox14;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.TextBox textBox13;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.RadioButton radioButton9;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.RadioButton radioButton8;
		private System.Windows.Forms.RadioButton radioButton10;
		private System.Windows.Forms.RadioButton radioButton16;
		private System.Windows.Forms.Button btnKeyConfigOK;
		private System.Windows.Forms.RadioButton radioButton5;
		private System.Windows.Forms.RadioButton radioButton15;
		private System.Windows.Forms.RadioButton radioButton11;
		private System.Windows.Forms.RadioButton radioButton7;
		private System.Windows.Forms.RadioButton radioButton14;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton12;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.RadioButton radioButton13;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lbl_1p;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.RadioButton rb_input2pCPU;
		private System.Windows.Forms.RadioButton rb_input2pPlayer;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RadioButton rb_input1pCPU;
		private System.Windows.Forms.RadioButton rb_input1pPlayer;
		private System.Windows.Forms.Button btn_folder;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cB_Chara1p;
		private System.Windows.Forms.Label lbl_Chara1p;
		private System.Windows.Forms.Label lbl_input;
		private System.Windows.Forms.CheckBox CH_Training;
		private System.Windows.Forms.CheckBox CH_Demo;
		private System.Windows.Forms.ComboBox cB_Chara2p;
		private System.Windows.Forms.CheckBox CH_StartBattle;
	}
}